#[macro_use]
extern crate gfx;

extern crate gfx_window_glutin;
extern crate glutin;

extern crate cgmath;

extern crate rand;

use gfx::traits::FactoryExt;
use gfx::Device;
use gfx_window_glutin as gfx_glutin;
use glutin::dpi::LogicalSize;
use glutin::GlContext;

use cgmath::{Matrix4, Ortho};

macro_rules! rgb {
    ($r:expr, $g:expr, $b:expr) => {[$r / 255., $g / 255., $b / 255.]}
}

macro_rules! rgba {
    ($r:expr, $g:expr, $b:expr, $a:expr) => {[$r / 255., $g / 255., $b / 255., $a]}
}

macro_rules! tri {
($x1:expr, $y1:expr, $x2:expr, $y2:expr, $x3:expr, $y3:expr, $color:expr) => {
    Triangle::new(($x1, $y1), ($x2, $y2), ($x3, $y3), $color)
}
}

pub type ColorFormat = gfx::format::Srgba8;
pub type DepthFormat = gfx::format::DepthStencil;


gfx_defines! {
    vertex Vertex {
        pos: [f32; 2] = "a_Pos",
        color: [f32; 3] = "a_Color",
    }

    pipeline pipe {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        out: gfx::RenderTarget<ColorFormat> = "Target0",
        view: gfx::Global<[[f32; 4]; 4]> = "u_view",
    }
}


#[derive(Debug, Clone, Copy)]
struct Triangle {
    pub a: (f32, f32),
    pub b: (f32, f32),
    pub c: (f32, f32),
    pub color: [f32; 3]
}

impl Triangle {
    pub fn new(a: (f32, f32), b: (f32, f32), c: (f32, f32), color: [f32; 3]) -> Self {
        return Triangle {
            a,
            b,
            c,
            color,
        }
    }
}

#[derive(Debug)]
struct RESF {
    background: Vec<Triangle>,
    letters: Vec<Vec<Triangle>>,
    tris: Vec<Triangle>
}

const TEXT_BG:[f32; 3] = rgb!(42.3, 26.2, 100.);
const TEXT: [f32; 3] = rgb!(56.1, 100., 100.);
const BG:[f32; 4] = rgba!(255., 255., 255., 1.0);

impl RESF {
    pub fn new() -> Self {
        let tris = vec![
            tri!(109., 118., 308., 119., 196., 278., rgb!(100., 54., 84.4)),
            tri!(123., 106. ,300., 154., 176., 271., rgb!(100., 22.9, 84.4)),
        ];

        let r = vec![
            tri!(82., 229., 91., 153., 110., 151., TEXT),
            tri!(82.,229., 110., 151., 104., 230., TEXT),
            tri!(110., 151., 152., 172., 105., 210., TEXT),
            tri!(105., 210., 123., 195., 121., 228., TEXT),
            tri!(121., 228., 123., 195., 156., 226., TEXT),
            tri!(105., 193., 105., 173., 120., 183., TEXT_BG),
        ];

        let e = vec![
            tri!(170., 156., 223., 162., 170., 185., TEXT),
            tri!(170., 165., 213., 198., 170., 215., TEXT),
            tri!(170., 196., 223., 227., 170., 227., TEXT),
        ];

        let s = vec![
            tri!(238., 186., 258., 158., 278., 159., TEXT),
            tri!(278., 159.,238., 186.,  248., 193., TEXT),
            tri!(248., 193., 257., 181., 259., 201., TEXT),
            tri!(259., 201., 257., 181., 268., 189., TEXT),
            tri!(234., 227., 268., 189., 282., 200., TEXT),
            tri!(234., 227., 282., 200., 263., 227., TEXT),
        ];

        let f = vec![
            tri!(292., 226., 292., 160., 312., 226., TEXT),
            tri!(292., 160., 312., 160., 312., 226., TEXT),
            tri!(312., 160., 327., 160., 312., 177., TEXT),
            tri!(312., 177., 329., 185., 312., 197., TEXT),

        ];

        let letters = vec![r, e, s, f];


        let background: Vec<Triangle> = vec![];
        return RESF {
            background,
            tris,
            letters
        }
    }

    pub fn get_vertices(&self) -> Vec<Vertex> {
        let mut vertices = vec![];


        let center = (200., 200.);
        let radius = 130.;
        let shift = 15.;
        
        let steps = 50;
        let angle_step = 360. / steps as f32;

        for step in 0..steps {
            let step = step as f32;


            //Left part of cog
            {
                let angle = (angle_step * step).to_radians();
                let angle_next = (angle_step * (step + 1.) - angle_step / 2.).to_radians();

                vertices.extend(&[
                    Vertex {
                        pos: [
                            f32::cos(angle_next) * radius + center.0,
                            f32::sin(angle_next) * radius + center.1,
                        ],
                        color: TEXT_BG
                    },
                    Vertex { pos: [center.0, center.1], color: TEXT_BG },
                    Vertex {
                        pos: [
                            f32::cos(angle) * (radius - shift) + center.0,
                            f32::sin(angle) * (radius - shift) + center.1,
                        ],
                        color: TEXT_BG
                    },
                ]);
            }

            //Part part of cog
            {
                let angle = (angle_step * step + angle_step / 2.).to_radians();
                let angle_next = (angle_step * (step + 1.)).to_radians();

                vertices.extend(&[
                    Vertex {
                        pos: [
                            f32::cos(angle_next) * (radius - shift) + center.0,
                            f32::sin(angle_next) * (radius - shift) + center.1,
                        ],
                        color: TEXT_BG
                    },
                    Vertex { pos: [center.0, center.1], color: TEXT_BG },
                    Vertex {
                        pos: [
                            f32::cos(angle) * (radius) + center.0,
                            f32::sin(angle) * (radius) + center.1,
                        ],
                        color: TEXT_BG
                    },
                ]);
            }
        }

        //TRIANGLES
        for tri in self.tris.iter() {
            vertices.extend(&[
                Vertex{pos: [tri.a.0, tri.a.1], color: tri.color},
                Vertex{pos: [tri.b.0, tri.b.1], color: tri.color},
                Vertex{pos: [tri.c.0, tri.c.1], color: tri.color}
            ]);
        }

        //RESF
        for letter in self.letters.iter() {
            for tri in letter.iter() {
                vertices.extend(&[
                    Vertex{pos: [tri.a.0, tri.a.1], color: tri.color},
                    Vertex{pos: [tri.b.0, tri.b.1], color: tri.color},
                    Vertex{pos: [tri.c.0, tri.c.1], color: tri.color}
                ]);
            }
        }
        vertices
    }
}



fn main() {

    let mut eventloop = glutin::EventsLoop::new();
    let builder = glutin::WindowBuilder::new()
        .with_title("Toy demo".to_string())
        .with_dimensions(LogicalSize::new(400.0, 400.0));

    let context = glutin::ContextBuilder::new().with_vsync(true);
    let (window, mut device, mut factory, main_color, _main_depth) =
        gfx_glutin::init::<ColorFormat, DepthFormat>(builder, context, &eventloop);
    let mut encoder: gfx::Encoder<_, _> = factory.create_command_buffer().into();
    let pso = factory.create_pipeline_simple(
        include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/shaders/rect_150.glslv")),
        include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/shaders/rect_150.glslf")),
        pipe::new(),
    ).unwrap();


    let resf = RESF::new();

    let vertices = resf.get_vertices();
    let (vertex_buffer, slice) = factory.create_vertex_buffer_with_slice(&vertices, ());


    let view = Matrix4::from(Ortho {
        left: 0.,
        right: 400.,
        bottom: 400.,
        top: 0.,
        near: 0.,
        far: 100.,
    });
    let data = pipe::Data {
        vbuf: vertex_buffer,
        out: main_color,
        view: view.into(),
    };

    let mut running = true;
    while running {
        eventloop.poll_events(|event| {
            use glutin::WindowEvent::*;
            if let glutin::Event::WindowEvent { event, .. } = event {
                match event {
                    CloseRequested |
                    KeyboardInput {
                        input: glutin::KeyboardInput {
                            virtual_keycode: Some(glutin::VirtualKeyCode::Escape), ..
                        }, ..
                    } => running = false,
                    _ => {}
                }
            }
        });

        encoder.clear(&data.out, BG);
        encoder.draw(&slice, &pso, &data);
        encoder.flush(&mut device);
        window.swap_buffers().unwrap();
        device.cleanup();
    }
}
